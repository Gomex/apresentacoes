# Palestra do GITLAB no NetCafé

Ela aconteceu no dia 30 de maio de 2014 às 11:00

Segue abaixo o vídeo para assistir a gravação:

<pendente link>

# Licença

Attribution-NonCommercial 4.0 International 

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>
